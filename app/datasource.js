var datasource = function(conf) {

	"use strict";

	const http = require('http');
	const fs = require('fs');
	const xmlparser = require('xml2json');
	const Emitter = require('events');

	class MyEmitter extends Emitter {}
	module.exports.event_emitter = new MyEmitter();

	// Check if local xml file exists
	fs.stat(conf.xml_local, (err, data) => {

		let today = Date.prototype.getDay.call((new Date));

		if (err || Math.floor(Date.parse(data.birthtime) / today) > 0) {
			// File is obsolete or doesn't exist, let's fetch new data
			console.log('Local XML file exists');
			_fetch_new_data();
		}
		else {
			// File is up to date
			console.log('Local XML file does not exist');
			_create_data_object();
		}
	});

	/**
	 * Fetch new currency data from remote endpoint and save in local xml
	 */
	function _fetch_new_data() {
		http.get(conf.xml_url, (ecb_res) => {
			if (ecb_res.statusCode < 400) {
				// Stream data to file
				let xml_file = fs.createWriteStream(conf.xml_local);
				ecb_res.pipe(xml_file);

				// Update JSON
				ecb_res.on('end', _create_data_object);
			}
			else {
				throw new Error('Could not fetch data from ECB server');
			}
		});
	}

	/**
	 * Update currency data object variable from local xml file
	 */
	function _create_data_object() {
		// Parse local xml file
		var xml_file = fs.readFileSync(conf.xml_local, 'utf8');

		console.log('Importing local XML file');

		// Parse XML and update currency_data object variable
		// todo try converting to map
		var obj = xmlparser.toJson(xml_file, {
			object: true,
			coerce: true
		});

		// Keep useful data only
		obj = obj['gesmes:Envelope'].Cube.Cube;

		// Emit ready event
		module.exports.event_emitter.emit('data', obj);
	}
};

module.exports.parseData = datasource;