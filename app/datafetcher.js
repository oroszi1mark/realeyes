
"use strict";

/**
 * Fetch exchange rate of a currency relative to EUR
 * @param {object} ecb_data Parsed XML containing conversion info
 * @param {string} currency Currency code
 * @returns {*} Exchange rate
 * @private
 */
function _getActualRate(ecb_data, currency) {

	if (currency == 'EUR') return 1;

	let filtered = ecb_data[0].Cube.find((entry) => entry.currency == currency);

	if (typeof filtered == 'object' && filtered.hasOwnProperty('rate')) {
		return filtered.rate;
	}
	else {
		return 0;
	}
}

/**
 * Fetch EUR exchange rate for a given day and currency
 * @param {object} day_rates Array of historical data
 * @param {string} currency Currency code
 * @returns {boolean|number} Exchange rate or false if params are omitted
 * @private
 */
function _getHistoryRate(day_rates, currency) {
	if (!day_rates || !currency) return false;

	if (currency == 'EUR') return 1;

	return day_rates.Cube.find((data) => data.currency == currency)['rate'];
}

/**
 * Convert amount of money from one currency to another at the actual rate
 * @param {object} ecb_data Parsed XML containing conversion info
 * @param {string} from Source currency code
 * @param {string} to Destination currency code
 * @param {number} amount Amount to convert
 * @returns {number} Converted amount
 */
function convertAmount(ecb_data, from, to, amount) {
	if (!ecb_data) return 0;

	amount = amount || 0; // default param vals not working

	console.log('Converting '+ amount +' '+ from +' to '+ to);

	// If FROM = EUR, simply return the rate
	if (from == 'EUR') return (_getActualRate(ecb_data, to) * amount);

	// If TO = EUR, simply return the rate
	if (to == 'EUR') return (1 / _getActualRate(ecb_data, from) * amount);

	// Fetch `from` exchange rate
	let from_amount = convertAmount(ecb_data, from, 'EUR', amount);

	// Fetch `to` exchange rate
	let result = convertAmount(ecb_data, 'EUR', to, from_amount);

	console.log('Result: '+ result +'\n---');

	return result;
}

/**
 * Return all available currencies based on current data
 * @param {object} ecb_data Parsed XML containing conversion info
 * @returns {object} Array of currency codes
 */
function getCurrencies(ecb_data) {
	let currencies = ['EUR'];

	// Use most recent data
	ecb_data[0].Cube.forEach((entry) => currencies.push(entry.currency));

	console.log(currencies.length +' currency codes returned in array');

	return currencies.sort();
}

/**
 * Return historical data of a pair of currencies
 * @param {object} ecb_data Parsed XML containing conversion info
 * @param {string} top Code of top currency, e.g. EUR from EUR/HUF
 * @param {string} bottom Code of bottom currency, e.g. EUR from EUR/HUF
 * @param {int} [limit=90] Number of days to show backwards from most recent day
 * @returns {boolean|map} Map of historical data or false if no ecb_data supplied
 */
function getHistory(ecb_data, top, bottom, limit) {
	if (!ecb_data) return false;

	var history = []; // wanted to use ES6 Map() at first but Rickshaw requires array
	limit = limit || 90; // default param vals not working

	console.log(`Generating history of ${top}/${bottom} for the last ${limit} days`);

	ecb_data.forEach((day) => {

		if (0 < limit--) {
			let date = new Date(day.time),
				_toprate,
				_bottomrate;


			// Find top currency EUR rate
			_toprate = _getHistoryRate(day, top);

			// Find bottom currency EUR rate
			_bottomrate = _getHistoryRate(day, bottom);

			// Add day to array with final currency conversion
			history.push({
				x: date.valueOf(),
				y: (1 / _toprate * _bottomrate)
			});
		}
	});

	// Preserve chronological order of data
	return history.reverse();
}

module.exports.getCurrencies = getCurrencies;
module.exports.convertAmount = convertAmount;
module.exports.getHistory = getHistory;