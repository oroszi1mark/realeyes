# Currency node

RealEyes test assignment.

## Packages used
* AngularJS
* Bootstrap
* Rickshaw
* Angular-Rickshaw
* ExpressJS
* XML2JSON


## Launch

1. Type `npm start` to start the server.
2. Visit `localhost:8080` in your browser.