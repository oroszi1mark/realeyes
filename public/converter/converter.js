"use strict";

angular
	.module('converter')
	.controller('ConverterController', ['$rootScope','$scope', '$http', function($rootScope, $scope, $http) {
		$scope.is_loading = true;
		$scope.input_options = {debounce: 200};
		$scope.amount = $scope.result = 0;

		// Handle for setting amounts
		$scope.updateForm = function() {

			// Request conversion
			if ($scope.from && $scope.to && $scope.amount > -1) {
				console.log('Converting amount');

				$http
					.get('convert/'+$scope.from+'/'+$scope.to+'/'+$scope.amount)
					.then((res) => {
						if (res.data.status == 'ok') {
							$scope.result = res.data.amount;
						}
						else {
							alert(res.data.msg);
						}
					}
				);
			}
		};

		// Handler for currency selectinos
		$scope.updateCurrency = function() {

			if ($scope.from && $scope.to) {
				// Request history
				console.log('Fetching history');
				$rootScope.$emit('hist_load');

				// Update form
				$scope.updateForm();
			}
		};

		// Request currencies
		$http
			.get('currencies/')
			.then((res) => {
				if (res.data.status == 'ok') {
					// Process data
					$scope.currencies = res.data.currencies;
					$scope.from = $scope.currencies[$scope.currencies.indexOf('EUR')];
					$scope.to = $scope.currencies[$scope.currencies.indexOf('HUF')];
					$scope.amount = 1;

					// Refresh graph
					$scope.updateCurrency.call();

					$scope.is_loading = false;
				}
				else {
					// Alert error
					alert(res.data.msg);
				}
			}
		);
	}])
	.directive('converter', function() {
		return {
			restrict: 'E',
			templateUrl: 'public/converter/converter.template.html',
			controller: 'ConverterController'
		};
	});