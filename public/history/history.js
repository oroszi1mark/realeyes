"use strict";

angular
	.module('converter')
	.controller('HistoryController', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
		$scope.is_ready = false;

		// Configure Rickshaw
		$scope.options = {
			renderer: 'area',
			min: 'auto'
		};
		$scope.features = {
			yAxis: 'formatKMBT',
			hover: {
				xFormatter: function(x) {
					return new Date(x).toLocaleString([], {day: '2-digit', month: 'short', year: 'numeric'});
				}
			}
		};
		$scope.series = [];

		$rootScope.$on('hist_load', () => {

			// Send request
			console.log('Requesting history');

			// Hide graph
			$scope.is_ready = false;

			$http
				.get('history/'+$scope.from+'/'+$scope.to)
				.then((res) => {
					if (res.data.status == 'ok') {
						// Store diagram data
						$scope.series = [{
							name: res.data.from +'/'+ res.data.to +' history',
							color: 'steelblue',
							data: res.data.history
						}];

						// Update axes
						$scope.features.hover.yFormatter = function(y) {
							return res.data.from +' '+ y.toFixed(2);
						};

						// Render graph
						$scope.is_ready = true;

						console.log(res.data);
					}
					else {
						// Alert error
						alert(res.data.msg);
					}
				}
			);
		});
	}])
	.directive('history', function() {
		return {
			restrict: 'E',
			templateUrl: 'public/history/history.template.html',
			controller: 'HistoryController'
		};
	});