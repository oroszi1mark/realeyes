
"use strict";

const http = require('http');
const fs = require('fs');

const express = require('express');
const app = express();

const datasource = require('./app/datasource.js');
const datafetcher = require('./app/datafetcher.js');

const CONF = {
	xml_url: {
		host: 'www.ecb.europa.eu',
		port: 80,
		method: 'GET',
		path: '/stats/eurofxref/eurofxref-hist-90d.xml'
	},
	xml_local: 'data/ecb_rates.xml'
};

// Parse XML data
datasource.parseData(CONF);

// Store ECB data object
var ecb_data;

datasource.event_emitter.once('data', (data) => {
	ecb_data = data;
	console.log('Data source is ready');
});

// Provide static route for resources
app.use('/angular', express.static(__dirname + '/node_modules/angular'));
app.use('/bootstrap', express.static(__dirname + '/node_modules/bootstrap'));
app.use('/d3', express.static(__dirname + '/node_modules/rickshaw/node_modules/d3'));
app.use('/rickshaw', express.static(__dirname + '/node_modules/rickshaw'));
app.use('/angular-rickshaw', express.static(__dirname + '/node_modules/angular-rickshaw'));
app.use('/public', express.static(__dirname + '/public'));

// Show main page
app.get('/', function(req, res) {
	res.sendFile(__dirname + '/public/index.html');
	console.log('Incoming request');
});

// Handle currency requests
app.get('/currencies', (req, res) => {
	console.log('Currency list requested');
	if (datafetcher && ecb_data) {
		res.sendStatus( JSON.stringify( {status: 'ok', currencies: datafetcher.getCurrencies(ecb_data)} ) );
	}
	else {
		console.log('Data unavailable');
		res.sendStatus( JSON.stringify( { status: 'error', msg: 'Our server is not yet ready, please try again later.' } ) );
	}
	res.end();
});

// Handle conversion requests
app.get('/convert/:from/:to/:amount', (req, res) => {
	console.log('Conversion requested');
	if (datafetcher && ecb_data) {
		let from = req.params.from,
			to = req.params.to,
			amount = req.params.amount,
			answer = JSON.stringify({
				status: 'ok',
				from: from,
				to: to,
				amount: datafetcher.convertAmount(ecb_data, from, to, amount)
			});
		res.sendStatus(answer);
	}
	else {
		console.log('Data unavailable');
		res.send(JSON.stringify({status:'error',msg:'Our server is not yet ready, please try again later.'}));
	}
	res.end();
});

// Handle history requests
app.get('/history/:from/:to', (req, res) => {
	console.log('History requested');
	if (datafetcher && ecb_data) {
		let from = req.params.from,
			to = req.params.to,
			history = datafetcher.getHistory(ecb_data, from, to),
			answer = JSON.stringify({
				status: 'ok',
				from: from,
				to: to,
				history: history
			});
		res.sendStatus(answer);
	}
	else {
		console.log('Data unavailable');
		res.send(JSON.stringify({status:'error',msg:'Our server is not yet ready, please try again later.'}));
	}
	res.end();
});

// Listen
app.listen(8080);
